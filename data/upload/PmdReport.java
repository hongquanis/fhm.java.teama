package fhm.pmd.teamA.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class PmdReport {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String content;
	
	private int beginline;
	
	private int endline;
	
	private int begincolumn;
	
	private int endcolumn;
	
	private String rule;
	
	private String ruleset;
	
	private String package_name;
	
	private String class_name;
	
	private String externalInfoUrl;
	
	private int priority;
	
	private String path;
	
	@ManyToOne
	private User user;

	public PmdReport(String content, int beginline, int endline, int begincolumn, int endcolumn, String rule,
			String ruleset, String package_name, String class_name, String externalInfoUrl, int priority, String path,
			User user) {
		super();
		this.content = content;
		this.beginline = beginline;
		this.endline = endline;
		this.begincolumn = begincolumn;
		this.endcolumn = endcolumn;
		this.rule = rule;
		this.ruleset = ruleset;
		this.package_name = package_name;
		this.class_name = class_name;
		this.externalInfoUrl = externalInfoUrl;
		this.priority = priority;
		this.path = path;
		this.user = user;
	}
}
