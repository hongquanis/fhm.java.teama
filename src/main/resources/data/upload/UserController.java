package fhm.pmd.teamA.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fhm.pmd.teamA.model.User;
import fhm.pmd.teamA.repository.UserRepository;
import fhm.pmd.teamA.service.UserService;

@Controller
public class UserController implements WebMvcConfigurer {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;

	@GetMapping("/register")
	public String registration(Model model, Principal principal) {
		User user = new User();
		model.addAttribute("user", user);

		return principal == null ? "register" : "redirect:/myUploadForm";
	}

	@PostMapping("/register")
	public String registration(@Valid User user, Model model) {
		User userExists = userRepository.findByUsername(user.getUsername());
		if (userExists != null) {
			model.addAttribute("error", "There is already a user registered with the username provided");
			return "register";
		}

		userService.save(user);
		model.addAttribute("successMessage", "User has been registered successfully");
		return "register";
	}

	@GetMapping("/login")
	public String login(Model model, HttpSession session, Principal principal) {
		return principal == null ? "login" : "redirect:/myUploadForm";
	}
}