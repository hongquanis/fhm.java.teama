package fhm.pmd.teamA.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import fhm.pmd.teamA.model.PmdReport;
import fhm.pmd.teamA.model.User;
import fhm.pmd.teamA.repository.PmdFileRepository;
import fhm.pmd.teamA.repository.PmdReportRepository;
import fhm.pmd.teamA.repository.PmdRepository;
import fhm.pmd.teamA.repository.UserRepository;
import fhm.pmd.teamA.service.PmdService;

@Controller
public class PmdController {
	@Autowired
	private PmdRepository pmdService;
	@Autowired
	private PmdReportRepository pmdReportRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PmdFileRepository pmdfileRepo;
	
	 @RequestMapping(value={"/pmd-list-file"})
	  public String listFile(Model model) {
	    model.addAttribute("listFile", pmdfileRepo.findAll());
	    return "pmd-list-file.html";
	  }
	 
	 @RequestMapping("/pmd-file/{fileID}")
		public String viewFile(@PathVariable Long fileID, Model model,Principal principal) {
		// Lấy các thông tin đăng nhập
			User user = userRepository.findByUsername(principal.getName());
			model.addAttribute("sort", "desc");
			
			model.addAttribute("listPMD", pmdReportRepository.findByFileIDAndUserId(fileID, user.getId()));

			return "pmd-list-all";
		}
	 
	@RequestMapping(value = { "/pmd-list" })
	public String listUser(Model model, Principal principal) {
		// Lấy các thông tin đăng nhập
		User user = userRepository.findByUsername(principal.getName());

		model.addAttribute("sort", "desc");
		model.addAttribute("listPMD",
				pmdReportRepository.findByUserIdOrderByFileIDAsc(user.getId()));
		return "pmdResult";
	}

	@GetMapping("/pmd-list/{fileID}/{sort}")
	public String Sort(Model model, Principal principal,@PathVariable Long fileID,
			@PathVariable String sort) {
		// Lấy các thông tin đăng nhập
		User user = userRepository.findByUsername(principal.getName());
		if (sort.equals("desc")) {
			model.addAttribute("listPMD", pmdReportRepository
					.findByFileIDAndUserIdOrderByPriorityDesc(fileID,user.getId()));
			model.addAttribute("sort", "asc");
		} else if (sort.equals("asc")) {
			model.addAttribute("listPMD", pmdReportRepository
					.findByFileIDAndUserIdOrderByPriorityAsc(fileID,user.getId()));
			model.addAttribute("sort", "desc");
		}

		return "pmd-list-sort";
	}

	@RequestMapping("/pmd-view/{id}")
	public String viewCustomer(@PathVariable int id, Model model) {
		PmdReport pmd = pmdService.findById(id);
		model.addAttribute("pmd", pmd);
		return "pmd-view";
	}
}
