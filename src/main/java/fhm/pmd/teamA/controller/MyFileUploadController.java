package fhm.pmd.teamA.controller;

import java.io.*;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import fhm.pmd.teamA.model.PmdFile;
import fhm.pmd.teamA.model.User;
import fhm.pmd.teamA.repository.PmdFileRepository;
import fhm.pmd.teamA.repository.UserRepository;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fhm.pmd.teamA.form.MyUploadForm;
import fhm.pmd.teamA.model.PmdReport;
import fhm.pmd.teamA.repository.PmdReportRepository;
import net.sourceforge.pmd.PMD;

@Controller
public class MyFileUploadController {
    // Define constant
    static final String PATH_DATA = System.getProperty("user.dir") + "/src/main/resources/data";
    static final String PATH_UPLOAD = System.getProperty("user.dir") + "/src/main/resources/data/upload";
    static final String PATH_REPORT = System.getProperty("user.dir") + "/src/main/resources/data/report";

    @Autowired
    PmdReportRepository pmdReportRepository;

    @Autowired
    PmdFileRepository pmdFileRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = {"/", "/myUploadForm"}, method = RequestMethod.GET)
    public String homePage(Model model) {
        MyUploadForm myUploadForm = new MyUploadForm();
        model.addAttribute("myUploadForm", myUploadForm);
        model.addAttribute("username", getUsername());

        return "uploadForm";
    }

    // POST: Processing upload and export report format xml
    @RequestMapping(value = "/processing", method = RequestMethod.POST)
    public String uploadOneFileHandlerPOST(HttpServletRequest request, //
                                           Model model, //
                                           @ModelAttribute("pmdAnalysisForm") MyUploadForm pmdAnalysisForm,
                                           Principal principal) {

        // Create path data if not exist
        File dataDir = new File(PATH_DATA);
        if (!dataDir.exists()) {
            dataDir.mkdir();
        }

        // Create path file upload if not exist
        File uploadRootDir = new File(PATH_UPLOAD);
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdir();
        }

        MultipartFile fileData = pmdAnalysisForm.getFileData();
        List<File> uploadedFiles = new ArrayList<File>();
        List<String> failedFiles = new ArrayList<String>();

        // Name's upload file in client
        String name = fileData.getOriginalFilename();

        //Define Path
        Long idFile = null;
        if (name != null && name.length() > 0) {
            try {
                // Create file in server
                File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(fileData.getBytes());
                stream.close();
                uploadedFiles.add(serverFile);

                // Create path report if not exist
                File outputDir = new File(PATH_REPORT);
                if (!outputDir.exists()) {
                    outputDir.mkdir();
                }

                // Set name for XML report file
                String nameXML = name.split("\\.")[0] + ".xml";
                String fileXML = outputDir.getPath() + "\\" + nameXML;
                String[] arguments = {"-d", serverFile.getPath(), "-f", "xml", "-R", "rulesets/java/quickstart.xml", "-r", fileXML};
                PMD.run(arguments);

                //Insert in pmd_file
                PmdFile pmdFile = new PmdFile();
                pmdFile.setFileName(serverFile.getName());
                pmdFileRepository.save(pmdFile);
                idFile = pmdFile.getId();

                // Read file xml and insert database
                try {
                    File inputFile = new File(fileXML);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(inputFile);
                    doc.getDocumentElement().normalize();
                    NodeList nList = doc.getElementsByTagName("violation");

                    //Get info user
                    User user = userRepository.findByUsername(principal.getName());

                    for (int i = 0; i < nList.getLength(); i++) {
                        Node nNode = nList.item(i);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) nNode;
                            PmdReport pmdObject = new PmdReport();
                            pmdObject.setContent(eElement.getTextContent());
                            pmdObject.setBeginLine(eElement.getAttribute("beginline"));
                            pmdObject.setEndLine(eElement.getAttribute("endline"));
                            pmdObject.setBeginColumn(eElement.getAttribute("begincolumn"));
                            pmdObject.setEndColumn(eElement.getAttribute("endcolumn"));
                            pmdObject.setRule(eElement.getAttribute("rule"));
                            pmdObject.setRuleSet(eElement.getAttribute("ruleset"));
                            pmdObject.setPackagePmd(eElement.getAttribute("package"));
                            pmdObject.setPmdClass(eElement.getAttribute("class"));
                            pmdObject.setExternalInfoUrl(eElement.getAttribute("externalInfoUrl"));
                            pmdObject.setPriority(eElement.getAttribute("priority"));
                            pmdObject.setPath(fileXML);
                            pmdObject.setFileID(idFile);
                            pmdObject.setUser(user);
                            pmdReportRepository.save(pmdObject);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("ERROR Save Data: " + e);
                    e.printStackTrace();
                }
            } catch (Exception e) {
                System.out.println("Error Write file: " + e);
                failedFiles.add(name);
            }
        }

        return "redirect:/pmd-result/" + idFile;
    }

    @RequestMapping(value = {"/pmd-result/{id_file}"})
    public String listUser(@PathVariable("id_file") long idFile, Model model) {
        ArrayList<PmdReport> result = pmdReportRepository.findByFileID(idFile);
        Optional<PmdFile> pmdFilesObject = pmdFileRepository.findById(idFile);
        model.addAttribute("listPMD", result);
        model.addAttribute("file_id", idFile);
        model.addAttribute("fileJava", pmdFilesObject.get().getFileName());
        return "pmdResult";

    }


    @RequestMapping(value = "/download/{id_file}")
    public void download(@PathVariable("id_file") Long idFile, HttpServletResponse response) throws IOException {
        try {
            ArrayList<PmdReport> result = pmdReportRepository.findByFileID(idFile);
            String path = result.get(0).getPath();
            File file = ResourceUtils.getFile(path);
            byte[] data = FileUtils.readFileToByteArray(file);
            // Config information response
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
            response.setContentLength(data.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getUsername() {
        // Get info login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

}
