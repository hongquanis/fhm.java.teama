package fhm.pmd.teamA.model;

import javax.persistence.*;

@Entity
@Table(name = "pmd_report", schema = "public")
public class PmdReport {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "content")
    private String content;

    @Column(name = "begin_line")
    private String beginLine;

    @Column(name = "end_line")
    private String endLine;

    @Column(name = "begin_column")
    private String beginColumn;

    @Column(name = "end_column")
    private String endColumn;

    @Column(name = "rule")
    private String rule;

    @Column(name = "rule_set")
    private String ruleSet;

    @Column(name = "package")
    private String packagePmd;

    @Column(name = "pmd_class")
    private String pmdClass;

    @Column(name = "external_Info_Url")
    private String externalInfoUrl;

    @Column(name = "priority")
    private String priority;

    @Column(name = "path")
    private String path;

    @ManyToOne
    private User user;

    @Column(name = "file_id")
    private Long fileID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBeginLine() {
        return beginLine;
    }

    public void setBeginLine(String beginLine) {
        this.beginLine = beginLine;
    }

    public String getEndLine() {
        return endLine;
    }

    public void setEndLine(String endLine) {
        this.endLine = endLine;
    }

    public String getBeginColumn() {
        return beginColumn;
    }

    public void setBeginColumn(String beginColumn) {
        this.beginColumn = beginColumn;
    }

    public String getEndColumn() {
        return endColumn;
    }

    public void setEndColumn(String endColumn) {
        this.endColumn = endColumn;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getRuleSet() {
        return ruleSet;
    }

    public void setRuleSet(String ruleSet) {
        this.ruleSet = ruleSet;
    }

    public String getPackagePmd() {
        return packagePmd;
    }

    public void setPackagePmd(String packagePmd) {
        this.packagePmd = packagePmd;
    }

    public String getPmdClass() {
        return pmdClass;
    }

    public void setPmdClass(String pmdClass) {
        this.pmdClass = pmdClass;
    }

    public String getExternalInfoUrl() {
        return externalInfoUrl;
    }

    public void setExternalInfoUrl(String externalInfoUrl) {
        this.externalInfoUrl = externalInfoUrl;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getFileID() {
        return fileID;
    }

    public void setFileID(Long fileID) {
        this.fileID = fileID;
    }

    public PmdReport(String content, String beginLine, String endLine, String beginColumn, String endColumn, String rule, String ruleSet, String packagePmd, String pmdClass, String externalInfoUrl, String priority, String path, User user, Long fileID) {
        this.content = content;
        this.beginLine = beginLine;
        this.endLine = endLine;
        this.beginColumn = beginColumn;
        this.endColumn = endColumn;
        this.rule = rule;
        this.ruleSet = ruleSet;
        this.packagePmd = packagePmd;
        this.pmdClass = pmdClass;
        this.externalInfoUrl = externalInfoUrl;
        this.priority = priority;
        this.path = path;
        this.user = user;
        this.fileID = fileID;
    }

    public PmdReport() {
    }

    @Override
    public String toString() {
        return "PmdReport{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", beginLine='" + beginLine + '\'' +
                ", endLine='" + endLine + '\'' +
                ", beginColumn='" + beginColumn + '\'' +
                ", endColumn='" + endColumn + '\'' +
                ", rule='" + rule + '\'' +
                ", ruleSet='" + ruleSet + '\'' +
                ", packagePmd='" + packagePmd + '\'' +
                ", pmdClass='" + pmdClass + '\'' +
                ", externalInfoUrl='" + externalInfoUrl + '\'' +
                ", priority='" + priority + '\'' +
                ", path='" + path + '\'' +
                ", user=" + user +
                ", fileID=" + fileID +
                '}';
    }
}
