package fhm.pmd.teamA.model;

import javax.persistence.*;

@Entity
@Table(name = "pmd_file",schema = "public")
public class PmdFile {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

   @Column(name = "file_name")
    private String fileName;

    public PmdFile(String fileName) {
        this.fileName = fileName;
    }

    public PmdFile() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
