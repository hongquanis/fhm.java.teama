package fhm.pmd.teamA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmdAnalysisSourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmdAnalysisSourceApplication.class, args);
	}
	
}
