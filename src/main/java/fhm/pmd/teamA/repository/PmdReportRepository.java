package fhm.pmd.teamA.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fhm.pmd.teamA.model.PmdReport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public interface PmdReportRepository  extends JpaRepository<PmdReport, Long> {
    Collection<PmdReport> findByUserId(Long id);
    
    Collection<PmdReport> findByUserIdOrderByFileIDAsc(Long id);

    Collection<PmdReport> findByUserIdOrderByPriorityAsc(Long id);
    
    Collection<PmdReport> findByUserIdOrderByPriorityDesc(Long id);

    ArrayList<PmdReport> findByFileID(Long id);
    
    Collection<PmdReport> findByFileIDAndUserId(Long fileId,Long userId);
    
    Collection<PmdReport> findByFileIDAndUserIdOrderByPriorityAsc(Long fileId,Long userId);
    
    Collection<PmdReport> findByFileIDAndUserIdOrderByPriorityDesc(Long fileId,Long userId);

    
}
