package fhm.pmd.teamA.repository;

import fhm.pmd.teamA.model.PmdFile;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PmdFileRepository extends JpaRepository<PmdFile, Long> {
	/**
	 * @author HungLH2 
	 * @return list file distinct
	 */
	@Query("select DISTINCT fileName from PmdFile")
	List<PmdFile> findAllDistinct();	
}
