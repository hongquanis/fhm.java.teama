package fhm.pmd.teamA.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fhm.pmd.teamA.model.PmdReport;

@Repository(value = "pmdRepository")
@Transactional(rollbackFor = Exception.class)

/**
 * 
 * @author HungLH2
 *
 */

public class PmdRepository {
	 @PersistenceContext  
	  private EntityManager entityManager;

	 public PmdReport findById(final int id) {
		    return entityManager.find(PmdReport.class, id);
		  }


	 
//	 public List<PmdReport> findAll() {
//		    return entityManager.createQuery("FROM PmdReport").getResultList();
//		  }
	 
//	  public void delete(final Pmd pmd) {
//	    entityManager.remove(pmd);
//	  }
	 
//	 public void persist(final Pmd pmd) {
//	    entityManager.persist(pmd);
//	  }
}