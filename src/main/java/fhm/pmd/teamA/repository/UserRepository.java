package fhm.pmd.teamA.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fhm.pmd.teamA.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
