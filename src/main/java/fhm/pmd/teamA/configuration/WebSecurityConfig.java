package fhm.pmd.teamA.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import fhm.pmd.teamA.service.UserDetailsServiceImpl;

// Xác định lớp này là một lớp cấu hình
@Configuration
// Kích hoạt việc tích hợp Spring Security với Spring MVC
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	// ta cần phải gọi đến UserDetailsServiceImpl để cấu hình
	private UserDetailsServiceImpl userDetailsServiceImpl;

	// Tạo bean để sử dụng
	// Giúp mã hóa password bằng thuật toán BCrypt
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	// Cấu hình xác thực
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder());
	}
	
	// Cấu hình phân quyền
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/resources/**", "/register").permitAll()
			.anyRequest().authenticated()
		.and()
			.formLogin()
			.loginPage("/login")
			.usernameParameter("username")
			.passwordParameter("password")
			.defaultSuccessUrl("/", true)
			.failureUrl("/login?error=true")
			.permitAll()
		.and()
			.rememberMe()
            .alwaysRemember(true)
        .and()
            .logout()
			.deleteCookies("JSESSIONID")
			.logoutSuccessUrl("/login");
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }
}
